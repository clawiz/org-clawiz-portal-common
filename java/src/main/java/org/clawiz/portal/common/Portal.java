/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common;

import org.clawiz.core.common.Core;

import org.clawiz.core.common.system.config.Config;
import org.clawiz.core.common.utils.RandomGUID;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.portal.common.engine.PortalEngine;
import org.clawiz.portal.common.engine.EngineContext;
import org.clawiz.portal.common.site.AbstractSiteContext;
import org.clawiz.portal.common.site.application.AbstractApplicationContext;

/**
 *
 */
public class Portal extends Service {


    public static final String CONFIG_PORTAL_ENGINE     = "engine";
    public static final String CONFIG_PORTAL_SITE       = "site";

    public static final String CONFIG_ENGINE_CLASS      = "class";

    public static final String CONFIG_SITE_NAME         = "name";
    public static final String CONFIG_SITE_PORT         = "port";
    public static final String CONFIG_SITE_DEFAULT_PORT = "80";
    public static final String CONFIG_SITE_CONTEXT      = "class";

    public static final String CONFIG_SITE_ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";

    public static final String CONFIG_SITE_APPLICATION_CONTEXT = "application";
    public static final String CONFIG_APPLICATION_CLASS        = "class";

    Config config;

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

/*
    public ServletContext prepareServletContext(Config config, AbstractSiteContext siteContext) {
        ServletContext context = new ServletContext();
        context.setSiteContext(siteContext);
        context.setConfig(config);

        String path      = config.getString(CONFIG_SERVLET_PATH);
        String className = config.getString(CONFIG_SERVLET_CLASS);

        if (StringUtils.isEmpty(className)) {
            throwException("Class not defined for servlet path '?' of site '?'", new Object[]{path, siteContext.getFullName()});
        }

        if (StringUtils.isEmpty(path)) {
            throwException("Path not defined for servlet class '?' of site '?'", new Object[]{className, siteContext.getFullName()});
        }

        context.setPath(path);
        context.setServletClassName(className);
        context.setServletClass(Core.getClassByName(className));

        context.prepare();
        return context;
    }

    public FilesServletContext prepareFilesContext(Config config, AbstractSiteContext siteContext) {
        FilesServletContext context = new FilesServletContext();
        context.setSiteContext(siteContext);
        context.setConfig(config);

        String path      = config.getString(CONFIG_FILES_PATH);
        String local     = config.getString(CONFIG_FILES_LOCAL);

        if (StringUtils.isEmpty(path)) {
            throwException("Path not defined for local files '?' of site '?'", new Object[]{local, siteContext.getFullName()});
        }

        if (StringUtils.isEmpty(path)) {
            throwException("Local not defined for files path  '?' of site '?'", new Object[]{path, siteContext.getFullName()});
        }

        context.setPath(path);
        context.setLocal(local);

        context.prepare();
        return context;
    }
*/

    public AbstractSiteContext prepareSiteContext(Config config, PortalContext portalContext) {

        String name             = config.getString(CONFIG_SITE_NAME);

        String contextClassName = config.getString(CONFIG_SITE_CONTEXT);
        if ( StringUtils.isEmpty(contextClassName)) {
            throwException("Site '?' value of 'class' attribute not defined", new Object[]{name});
        }

        AbstractSiteContext siteContext = null;
        try {
            siteContext = (AbstractSiteContext) getService(Core.getClassByName(contextClassName), contextClassName + ":" + (new RandomGUID()).toString() );
        } catch (Exception e) {
            e.printStackTrace();
            throwException("Site '?' context class '?' instantiation exception : ?'", new Object[]{name, contextClassName, e.getMessage()}, e);
        }

        siteContext.setPortalContext(portalContext);
        siteContext.setConfig(config);
        siteContext.setSiteName(name);

        String portString       = config.getString(CONFIG_SITE_PORT);
        if ( StringUtils.isEmpty(portString)) {
            portString = CONFIG_SITE_DEFAULT_PORT;
        }
        siteContext.setHttpPort(StringUtils.toBigDecimal(portString).intValue());

        siteContext.setAccessControlAllowOrigin(config.getString(CONFIG_SITE_ACCESS_CONTROL_ALLOW_ORIGIN));


        for(Config applicationConfig : config.getConfigList(CONFIG_SITE_APPLICATION_CONTEXT)) {
            String className = applicationConfig.getString(CONFIG_APPLICATION_CLASS, false);
            if ( StringUtils.isEmpty(className) ) {
                throwException("Application class not defined for site '?' application '?'", new Object[]{siteContext.getFullName(), applicationConfig.getString(AbstractApplicationContext.CONFIG_NAME, "NULL")});
            }
            AbstractApplicationContext applicationContext = null;

            try {
                applicationContext = (AbstractApplicationContext) getService(Core.getClassByName(className), className + ":" + (new RandomGUID()).toString());
            } catch (Exception e) {
                e.printStackTrace();
                throwException("Site '?' application class '?' instantiation exception : ?'", new Object[]{name, className, e.getMessage()}, e);
            }
            applicationContext.setConfig(applicationConfig);
            siteContext.addApplication(applicationContext);
        }

        Config dataSourceConfig = config.getConfig("datasource");
        if ( dataSourceConfig != null ) {
            Config connectionConfig = dataSourceConfig.getConfig("connection");
            if ( connectionConfig != null ) {
                siteContext.getDataSourceContext().getConnectionContext().setCloseAfterCall(StringUtils.toBoolean(connectionConfig.getString("close-after-call", "true")));
                siteContext.getDataSourceContext().getConnectionContext().setCommitOnSuccess(StringUtils.toBoolean(connectionConfig.getString("commit-on-success", "true")));
                siteContext.getDataSourceContext().getConnectionContext().setRollbackOnFailure(StringUtils.toBoolean(connectionConfig.getString("rollback-on-failure", "true")));
            }
        }

        siteContext.prepare();
        siteContext.prepareApplications();

        portalContext.addSite(siteContext);

        return siteContext;
    }

    public EngineContext prepareEngineContext(Config config, PortalContext portalContext) {
        EngineContext context = getService(EngineContext.class, EngineContext.class.getName() + ":" + (new RandomGUID()).toString());
        context.setPortalContext(portalContext);

        String engineClassName = config.getString(CONFIG_ENGINE_CLASS, true);
        context.setEngineClassName(engineClassName);
        PortalEngine engine = ((PortalEngine) getService(Core.getClassByName(engineClassName), engineClassName + ":" + (new RandomGUID()).toString()));
        engine.setPortalContext(portalContext);

        context.setEngine(engine);

        context.prepare();
        return context;
    }

    private int portalsCount = 0;
    public PortalContext preparePortalContext(Config config) {

        PortalContext portalContext = getService(PortalContext.class, PortalContext.class.getName() + ":" + portalsCount++);
        portalContext.setConfig(config);

        portalContext.setEngineContext(prepareEngineContext(config.getConfig(CONFIG_PORTAL_ENGINE), portalContext));

        for(Config siteConfig : config.getConfigList(CONFIG_PORTAL_SITE)) {
            AbstractSiteContext siteContext = prepareSiteContext(siteConfig, portalContext);
        }

        portalContext.prepare();
        return portalContext;

    }

    public void start() {

        PortalContext portalContext = preparePortalContext(getConfig());

        portalContext.getEngineContext().getEngine().start();

        PortalSessionTimeoutDispatcher timeoutDispatcher = Core.newDispatcherSession(PortalSessionTimeoutDispatcher.class.getName()).getService(PortalSessionTimeoutDispatcher.class, (new RandomGUID()).toString());
        timeoutDispatcher.setPortalContext(portalContext);
        timeoutDispatcher.start();

    }
}
