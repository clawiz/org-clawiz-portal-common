/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common;


import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.utils.RandomGUID;
import org.clawiz.portal.common.engine.EngineContext;
import org.clawiz.portal.common.site.AbstractSiteContext;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class PortalContext extends AbstractContext {

    public static final String CONFIG_PORT = "port";
    public static final int DEFAULT_PORT = 80;

    @NotInitializeService
    private EngineContext engineContext;
    ArrayList<PortalPortContext> ports = new ArrayList<>();

    public EngineContext getEngineContext() {
        return engineContext;
    }

    public void setEngineContext(EngineContext engineContext) {
        this.engineContext = engineContext;
    }

    private int portsCount = 0;
    public PortalPortContext addSite(AbstractSiteContext site) {

        if ( site.getHttpPort() == 0 ) {
            site.setHttpPort(DEFAULT_PORT);
        }

        PortalPortContext portContext = null;
        for (PortalPortContext port : ports ) {
            if ( port.getPort() == site.getHttpPort()) {
                portContext = port;
                break;
            }
        }

        if ( portContext == null ) {
            portContext = getService(PortalPortContext.class, PortalPortContext.class.getName() + ":" + (new RandomGUID()).toString());
            portContext.setPortalContext(this);
            portContext.setPort(site.getHttpPort());
            ports.add(portContext);
        }

        portContext.addSite(site);
        return portContext;
    }

    public ArrayList<PortalPortContext> getPorts() {
        return ports;
    }

    public String getName() {
        return engineContext != null ? engineContext.getEngineClassName() : this.toString();
    }

    public  String getSessionIdCookieName() {
        return "CWSESSIONID";
    }

    private ConcurrentHashMap<String, PortalSession> sessionsCache = new ConcurrentHashMap<>();

    public PortalSession getSession(String id) {
        return id != null ? sessionsCache.get(id) : null;
    }




}
