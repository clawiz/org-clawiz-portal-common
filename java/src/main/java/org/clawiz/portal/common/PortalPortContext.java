/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common;


import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.portal.common.site.AbstractSiteContext;

import java.util.ArrayList;

/**
 *
 */
@SuppressWarnings("WeakerAccess")
public class PortalPortContext extends AbstractContext {

    private int port;

    @NotInitializeService
    private
    PortalContext                  portalContext;
    private ArrayList<AbstractSiteContext> sites = new ArrayList<>();

    public void addSite(AbstractSiteContext site) {
        sites.add(site);
    }

    public ArrayList<AbstractSiteContext> getSites() {
        return sites;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @SuppressWarnings("unused")
    public PortalContext getPortalContext() {
        return portalContext;
    }

    public void setPortalContext(PortalContext portalContext) {
        this.portalContext = portalContext;
    }

}
