/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common;


import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.storage.remotenode.RemoteNodeObject;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.portal.common.servlet.AbstractServlet;
import org.clawiz.portal.common.servlet.ServletContext;
import org.clawiz.portal.common.site.AbstractSiteContext;

import javax.servlet.http.Cookie;
import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class PortalSession {

    private Session             coreSession;

    private Cookie              sessionCookie;

    private AbstractSiteContext siteContext;

    private long                lastActivityTime;

    public PortalSession(Session coreSession) {
        this.coreSession = coreSession;
        lastActivityTime = (new Date()).getTime();

    }

    @SuppressWarnings("WeakerAccess")
    public long getLastActivityTime() {
        return lastActivityTime;
    }

    public String getKey() {
        return coreSession.getKey();
    }

    public Cookie getSessionCookie() {
        return sessionCookie;
    }

    public void setSessionCookie(Cookie sessionCookie) {
        this.sessionCookie = sessionCookie;
    }

    public AbstractSiteContext getSiteContext() {
        return siteContext;
    }

    public void setSiteContext(AbstractSiteContext siteContext) {
        this.siteContext = siteContext;
    }

    private final ConcurrentHashMap<String, AbstractServlet> servletsCache = new ConcurrentHashMap<>();
    public <T extends AbstractServlet> T lockServlet(ServletContext servletContext) {
        lastActivityTime = (new Date()).getTime();


        if ( servletContext.getServletClass() == null ) {
            throw new CoreException("Servlet class nod defined in servlet context ? at path ?", servletContext.toString(), servletContext.getPath());
        }

        String servletKey = servletContext.getServletClass().getName() + "/" + servletContext.getPath();
        AbstractServlet servlet;
        synchronized (servletsCache ) {
            servlet = servletsCache.get(servletKey);
            if ( servlet == null ) {
                servlet = (AbstractServlet) coreSession.getService(servletContext.getServletClass());
                servletsCache.put(servletKey, servlet);
            }
        }

        if ( servlet.isAllowedSessionConcurrentCalls() ) {
            //noinspection unchecked
            return (T) servlet;
        }

        boolean lockObtained = false;
        while (! lockObtained ) {
            //noinspection SynchronizationOnLocalVariableOrMethodParameter
            synchronized (servlet) {
                if ( ! servlet.isLocked() ) {
                    servlet.getLock();
                    lockObtained = true;
                }
            }
            if ( ! lockObtained ) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        //noinspection unchecked
        return (T) servlet;
    }

    public void logout() {
        coreSession.logout();
        servletsCache.clear();
    }

    public boolean login(String userName, String password) {
        logout();
        return coreSession.login(userName, password);
    }

    public boolean isRegistered() {
        return coreSession.isRegistered();
    }

    public Session getCoreSession() {
        return coreSession;
    }
}
