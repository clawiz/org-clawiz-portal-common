/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.engine.jetty;


import org.clawiz.portal.common.PortalPortContext;
import org.clawiz.portal.common.engine.PortalEngine;
import org.clawiz.portal.common.engine.jetty.handler.JettySiteNotFoundHandler;
import org.clawiz.portal.common.engine.jetty.handler.JettySiteServletHandler;
import org.clawiz.portal.common.servlet.FilesServletContext;
import org.clawiz.portal.common.site.AbstractSiteContext;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.util.resource.Resource;

import java.io.File;

public class JettyPortalEngine extends PortalEngine {

    @Override
    public void start() {


        for (PortalPortContext portContext : getPortalContext().getPorts()) {
            Server server = new Server();

            HttpConfiguration httpConfig = new HttpConfiguration();

            ServerConnector   connector = new ServerConnector(server, new HttpConnectionFactory(httpConfig));

            connector.setPort(portContext.getPort());
            connector.setIdleTimeout(30000);

            server.addConnector(connector);

            HandlerList serverHandlers = new HandlerList();
            server.setHandler(serverHandlers);

            ContextHandlerCollection serverContexts = new ContextHandlerCollection();
            serverHandlers.addHandler(serverContexts);

            for (AbstractSiteContext siteContext : portContext.getSites()) {

                for( FilesServletContext fc : siteContext.getFiles() ) {
                    ContextHandler contextHandler = new ContextHandler();
                    contextHandler.setContextPath(fc.getPath());
                    serverContexts.addHandler(contextHandler);

                    if ( fc.isUseClassLoader() ) {
                        ResourceHandler rh = new ResourceHandler();
                        contextHandler.setHandler(rh);

                        rh.setBaseResource(Resource.newClassPathResource(fc.getLocalPath()));
                        rh.setDirectoriesListed(fc.isDirectoriesListed());
                    } else {
                        ResourceHandler rh = new ResourceHandler();
                        contextHandler.setHandler(rh);

                        rh.setBaseResource(Resource.newResource(new File(fc.getLocalPath()) ));
                        rh.setDirectoriesListed(fc.isDirectoriesListed());

                    }
                }

                JettySiteServletHandler servletContextHandler = new JettySiteServletHandler();
                servletContextHandler.setSiteContext(siteContext);
                serverHandlers.addHandler(servletContextHandler);

            }

            if( portContext.getSites().size() > 0 ) {
                JettySiteNotFoundHandler notFoundHandler = new JettySiteNotFoundHandler();
                notFoundHandler.setSiteContext(portContext.getSites().get(0));
                serverHandlers.addHandler(notFoundHandler);
            }


            try {
                server.start();
//                server.join();
            } catch (Exception e) {
                throwException("Exception on start jetty server ?", new Object[]{e.getMessage()}, e);
            }
        }


    }
}
