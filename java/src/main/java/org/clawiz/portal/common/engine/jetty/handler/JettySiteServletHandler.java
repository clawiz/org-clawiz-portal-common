/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.engine.jetty.handler;


import org.clawiz.core.common.CoreException;
import org.clawiz.portal.common.site.AbstractSiteContext;
import org.eclipse.jetty.server.Request;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JettySiteServletHandler extends AbstractJettyHandler {

    AbstractSiteContext siteContext;

    public AbstractSiteContext getSiteContext() {
        return siteContext;
    }

    JettySiteServletHandlerService handlerService;

    public JettySiteServletHandlerService getHandlerService() {
        if ( handlerService == null ) {
            handlerService = siteContext.getService(JettySiteServletHandlerService.class);
        }
        return handlerService;
    }

    public void setSiteContext(AbstractSiteContext siteContext) {
        this.siteContext = siteContext;
    }

    @Override
    public void handle(String target,
                       Request request,
                       HttpServletRequest httpServletRequest,
                       HttpServletResponse httpServletResponse) throws IOException, ServletException {

        try {
            getHandlerService().handle(siteContext, target, request, httpServletRequest, httpServletResponse);
        } catch (CoreException e) {
            throw new ServletException(e.getMessage(), e);
        }
    }

}
