/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.engine.jetty.handler;


import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.portal.common.PortalSession;
import org.clawiz.portal.common.engine.jetty.JettyHttpRequestContext;
import org.clawiz.portal.common.engine.jetty.JettyHttpResponseContext;
import org.clawiz.portal.common.servlet.FilesServletContext;
import org.clawiz.portal.common.servlet.ServletContext;
import org.clawiz.portal.common.servlet.http.AbstractHttpServlet;
import org.clawiz.portal.common.servlet.http.error.AbstractHttpErrorHandlerServlet;
import org.clawiz.portal.common.servlet.http.error.DefaultHttpErrorHandlerServlet;
import org.clawiz.portal.common.servlet.http.protocol.Header;
import org.clawiz.portal.common.servlet.http.protocol.HttpResponseStatus;
import org.clawiz.portal.common.site.AbstractSiteContext;
import org.clawiz.portal.common.site.LocaleContext;
import org.clawiz.portal.common.site.application.AbstractApplicationContext;
import org.eclipse.jetty.server.Request;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

public class JettySiteServletHandlerService extends Service {


    private PortalSession newSession(AbstractSiteContext siteContext) {
        PortalSession  portalSession = siteContext.newSession();
        if ( siteContext.getSiteAutoLoginUserName() != null ) {
            portalSession.login(siteContext.getSiteAutoLoginUserName(), siteContext.getSiteAutoLoginUserPassword());
        }
        return portalSession;
    }


    protected PortalSession getPortalSession(HttpServletRequest  httpServletRequest, AbstractSiteContext siteContext, ServletContext servletContext, JettyHttpResponseContext responseContext) {

        String  sessionId           = httpServletRequest.getParameter(siteContext.getSessionIdParameterName());
        boolean sessionAsParameter  = sessionId != null;



        if ( sessionId == null ) {
            if (  httpServletRequest.getCookies() != null ) {
                for(Cookie cookie : httpServletRequest.getCookies() ) {
                    if ( siteContext.getSessionIdParameterName().equals(cookie.getName())) {
                        sessionId = cookie.getValue();
                        break;
                    }
                }
            }
        }

        if ( servletContext != null && servletContext.isSessionIdRequired() && StringUtils.isEmpty(sessionId) ) {
            throwException("Parameter '?' must be defined", siteContext.getSessionIdParameterName());
        }

        PortalSession portalSession = siteContext.getSession(sessionId);
        if ( portalSession == null ) {
            if ( sessionId != null ) {
                if ( sessionAsParameter ) {
                    throwException("Wrong session id '?'", sessionId);
                }
                logDebug("Session id '" + sessionId + "' not found in sessions cache");
            }
            portalSession = newSession(siteContext);
            responseContext.setCookie(portalSession.getSessionCookie());
        }
        return portalSession;
    }

    private static ConcurrentHashMap<Session, BigDecimal> sessionCountersCache = new ConcurrentHashMap<>();
    private void incrementSessionCounter(Session session) {
        synchronized ( session ) {
            BigDecimal counter = sessionCountersCache.get(session);
            sessionCountersCache.put(session, counter != null ? new BigDecimal(counter.intValue()+1) : new BigDecimal(1));
        }

    }
    private void decrementSessionCounter(AbstractSiteContext siteContext, Session session) {
        synchronized (session) {
            BigDecimal counter = sessionCountersCache.get(session);
            if(  counter == null || counter.intValue() <= 1 ) {
                sessionCountersCache.remove(session);
                if ( siteContext.getDataSourceContext().getConnectionContext().isCloseAfterCall()) {
                    session.getConnection().close();
                }
            } else {
                sessionCountersCache.put(session, new BigDecimal(counter.intValue()-1));
            }
        }
    }

    private void callServlet(
                            AbstractSiteContext      siteContext,
                            String                   target,
                            LocaleContext            localeContext,
                            ServletContext           servletContext,
                            Request                  jettyRequest,
                            HttpServletRequest       httpServletRequest,
                            HttpServletResponse      httpServletResponse,
                            JettyHttpRequestContext  requestContext,
                            JettyHttpResponseContext responseContext,
                            HttpResponseStatus       errorStatus,
                            String                   message
                            ) {


        if ( servletContext == null ) {
            return;
        }

        if ( localeContext == null ) {
            localeContext = siteContext.getDefaultLocale();
        }

        requestContext.setResponse(responseContext);
        responseContext.setRequest(requestContext);

        requestContext.setHttpServletRequest(httpServletRequest);
        requestContext.setJettyRequest(jettyRequest);

        requestContext.setPath(siteContext.removeLocaleFromPath(target, localeContext));

        responseContext.setHttpServletResponse(httpServletResponse);

        requestContext.setServletContext(servletContext);
        requestContext.setLocaleContext(localeContext);

        requestContext.prepare();
        responseContext.prepare();

        AbstractHttpServlet servlet = requestContext.getPortalSession().lockServlet(servletContext);
        incrementSessionCounter(servlet.getSession());
        try {
            if ( errorStatus == null ) {
                servlet.service(requestContext);
            } else {
                ((AbstractHttpErrorHandlerServlet) servlet).doError(requestContext, errorStatus, message);
                servlet.rollback();
            }

            if ( siteContext.getDataSourceContext().getConnectionContext().isCommitOnSuccess() ) {
                servlet.commit();
            }
            decrementSessionCounter(siteContext, servlet.getSession());
        } catch (Exception e) {
            if ( siteContext.getDataSourceContext().getConnectionContext().isRollbackOnFailure() ) {
                servlet.rollback();
            }
            decrementSessionCounter(siteContext, servlet.getSession());
            throw e;
        } finally {
            servlet.releaseLock();
        }

        httpServletResponse.setContentType(responseContext.getContentType() + ";charset=" + responseContext.getCharset());
        httpServletResponse.setStatus(responseContext.getStatus().code());

        for (Cookie cookie : responseContext.getCookies().values()) {
            httpServletResponse.addCookie(cookie);
        }

        for (Header header : responseContext.getHeaders().values()) {
            httpServletResponse.addHeader(header.getName(), header.getValue());
        }

        if ( siteContext.getAccessControlAllowOrigin() != null ) {
            httpServletResponse.addHeader("Access-Control-Allow-Origin", siteContext.getAccessControlAllowOrigin());
        }

        try {
            PrintWriter out = httpServletResponse.getWriter();
            out.println(responseContext.getBuffer().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        jettyRequest.setHandled(true);

    }

    public void handleError(AbstractSiteContext      siteContext,
                            String                   target,
                            Request                  jettyRequest,
                            HttpServletRequest       httpServletRequest,
                            HttpServletResponse      httpServletResponse,
                            JettyHttpRequestContext  requestContext,
                            JettyHttpResponseContext responseContext,
                            HttpResponseStatus       errorStatus,
                            String                   message) {

        LocaleContext              localeContext      = siteContext.getLocaleByPath(target);
        String                     applicationPath    = null;
        ServletContext             handlerContext     = null;

        String                     path               = siteContext.removeLocaleFromPath(target, localeContext);

        for(AbstractApplicationContext context : siteContext.getApplicationContexts() ) {

            if ( StringUtils.isLike(path,       context.getPath() + "*")  ||
                 StringUtils.isLike(path,       context.getPath() + "/*") ||
                 StringUtils.isLike(path, "/" + context.getPath() + "*")  ||
                 StringUtils.isLike(path, "/" + context.getPath() + "/*")
                    ) {
                if ( applicationPath == null ||
                        (context.getPath() != null && context.getPath().length() > applicationPath.length())) {
                    applicationPath    = context.getPath();
                    handlerContext     = context.getHttpErrorHandler();
                }
            }
        }


        if ( handlerContext == null ) {
            handlerContext = new ServletContext();
            handlerContext.setServletClass(DefaultHttpErrorHandlerServlet.class, getSession());
        }

        callServlet(siteContext, target, localeContext, handlerContext, jettyRequest, httpServletRequest, httpServletResponse,
                requestContext, responseContext,
                errorStatus,
                message);

        jettyRequest.setHandled(true);

    }

    protected String encodeAttributeValueQuotes(String str) {
        return str.replace("\"","&quot;");
    }

    public void handle(AbstractSiteContext siteContext,
                       String              target,
                       Request             jettyRequest,
                       HttpServletRequest  httpServletRequest,
                       HttpServletResponse httpServletResponse) {

        LocaleContext  localeContext  = siteContext.getLocaleByPath(target);
        ServletContext servletContext = siteContext.getServletContextByPath(target, localeContext);

        if ( servletContext == null ) {
            return;
        }

        // Files handled by Jetty Resource and Context handlers
        if ( servletContext instanceof FilesServletContext ) {
            return;
        }

        JettyHttpRequestContext  requestContext;
        JettyHttpResponseContext responseContext = new JettyHttpResponseContext();
        PortalSession            portalSession   = null;
        try {

            requestContext  = new JettyHttpRequestContext();

            portalSession   = getPortalSession(httpServletRequest, siteContext, servletContext, responseContext);

            requestContext.setPortalSession(portalSession);

            callServlet(siteContext
                    , target
                    , localeContext
                    , servletContext
                    , jettyRequest
                    , httpServletRequest
                    , httpServletResponse
                    , requestContext
                    , responseContext
                    , null
                    , null);

        } catch (Exception e) {
            e.printStackTrace();
            try {
                rollback();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            JettyHttpRequestContext  errorRequestContext  = new JettyHttpRequestContext();
            JettyHttpResponseContext errorResponseContext = new JettyHttpResponseContext();
            errorRequestContext.setPortalSession(portalSession != null ? portalSession : newSession(siteContext));
            handleError(siteContext, target, jettyRequest, httpServletRequest, httpServletResponse,
                    errorRequestContext, errorResponseContext,
                    HttpResponseStatus.INTERNAL_SERVER_ERROR,
                    e.getMessage() != null ? e.getMessage() : e.toString());

        } finally {
            if ( portalSession != null ) {
//                portalSession.getCoreSession().getConnection().close();
            }
        }
    }

}
