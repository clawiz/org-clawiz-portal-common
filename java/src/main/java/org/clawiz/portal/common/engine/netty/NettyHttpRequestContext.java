/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.engine.netty;


import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.portal.common.servlet.ServletContext;
import org.clawiz.portal.common.servlet.http.protocol.HttpMethod;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;
import org.clawiz.portal.common.site.AbstractSiteContext;
import org.clawiz.portal.common.site.LocaleContext;

import javax.servlet.http.Cookie;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class NettyHttpRequestContext extends AbstractHttpRequestContext {

    HttpMethod              method;

    String                  contentType;
    String                  charset;

    ServletContext          servletContext;
    LocaleContext           localeContext;

    String uri;
    String path;

    HashMap<String, String> headers = new HashMap<>();
    HashMap<String, String[]> parameters = new HashMap<>();

    String protocolVersionString;

    public static final String CONTENT_TYPE_TEXTHTML = "text/html";

    HashMap<String, Cookie> cookies = new HashMap<>();

    public void setCookie(Cookie cookie) {
        cookies.put(cookie.getName(), cookie);
    }

    public HttpMethod getMethod() {
        return method;
    }

    public void setMethod(HttpMethod method) {
        this.method = method;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public String getHeader(String header) {
        return header != null ? headers.get(header) : null;
    }

    public HashMap<String, String[]> getParametersHashMap() {
        return parameters;
    }

    public String[] getParameterValues(String parameter) {
        return parameter != null ? parameters.get(parameter) : null;
    }

    public String getParameterValue(String parameter) {
        String[] list = parameter != null ? parameters.get(parameter) : null;
        if ( list == null ) {
            return null;
        }
        if ( list != null && list.length > 1) {
            throw new CoreException("Parameter '?' has more than one value", parameter);
        }
        return list.length > 0 ? list[0] : null;
    }

    @Override
    public String getParameterValue(String parameter, boolean throwErrorOnNotFound) {
        String value = getParameterValue(parameter);
        if ( throwErrorOnNotFound && StringUtils.isEmpty(value)) {
            throw new CoreException("Parameter '?' must be defined", parameter);
        }
        return value;
    }

    public Cookie getCookie(String name) {
        return name != null ? cookies.get(name) : null;
    }

    public String getCookieValue(String name) {
        if ( name == null ) {
            return null;
        }
        Cookie cookie = cookies.get(name);
        return cookie != null ? cookie.getValue() : null;
    }

    public AbstractSiteContext getSiteContext() {
        return getSiteContext();
    }

    public ServletContext getServletContext() {
        return servletContext;
    }

    public LocaleContext getLocaleContext() {
        return localeContext;
    }

    public String getLanguage() {
        return localeContext != null ? localeContext.getLanguage() : null;
    }


}
