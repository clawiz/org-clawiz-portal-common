/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.engine.netty;




import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.clawiz.portal.common.PortalPortContext;
import org.clawiz.portal.common.engine.PortalEngine;
import org.clawiz.portal.common.engine.netty.http.NettyServerHttpInitializer;


/**
 *
 */
public class NettyPortalEngine extends PortalEngine {

    public void start() {

        ServerBootstrap b = new ServerBootstrap();

        for ( PortalPortContext portContext : getPortalContext().getPorts() ) {

            EventLoopGroup bossGroup = new NioEventLoopGroup();
            EventLoopGroup workerGroup = new NioEventLoopGroup();

            try {

                NettyServerHttpInitializer portInitializer = new NettyServerHttpInitializer();
                portInitializer.setPortContext(portContext);

                b.group(bossGroup, workerGroup)
                        .channel(NioServerSocketChannel.class)
//                        .handler(new LoggingHandler(LogLevel.DEBUG))
                        .childHandler(portInitializer);

                Channel ch = b.bind(portContext.getPort()).sync().channel();
                ch.closeFuture().sync();

            } catch (Exception e ) {
                throwException("Exception on netty initialization ?", new Object[]{e.getMessage()}, e);
            } finally {
                bossGroup.shutdownGracefully();
                workerGroup.shutdownGracefully();
            }


        }

    }


}


