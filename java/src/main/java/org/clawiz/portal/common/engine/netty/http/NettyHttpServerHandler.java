/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.engine.netty.http;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.DecoderResult;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.cookie.*;
import io.netty.handler.codec.http.cookie.DefaultCookie;
import io.netty.handler.codec.http.cookie.ServerCookieEncoder;
import io.netty.handler.codec.http.multipart.Attribute;
import io.netty.handler.codec.http.multipart.DefaultHttpDataFactory;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import io.netty.util.CharsetUtil;
import org.clawiz.core.common.Core;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.logger.Logger;
import org.clawiz.portal.common.PortalPortContext;
import org.clawiz.portal.common.PortalSession;
import org.clawiz.portal.common.engine.netty.NettyHttpRequestContext;
import org.clawiz.portal.common.engine.netty.NettyHttpResponseContext;
import org.clawiz.portal.common.servlet.AbstractRequestFailContext;
import org.clawiz.portal.common.servlet.AbstractServlet;
import org.clawiz.portal.common.servlet.FilesServletContext;
import org.clawiz.portal.common.servlet.ServletContext;
import org.clawiz.portal.common.servlet.http.protocol.*;
import org.clawiz.portal.common.servlet.http.AbstractHttpServlet;
import org.clawiz.portal.common.servlet.http.protocol.HttpMethod;
import org.clawiz.portal.common.site.AbstractSiteContext;

import javax.servlet.http.Cookie;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static io.netty.handler.codec.http.HttpHeaders.Names.*;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

/**
 *
 */

public class NettyHttpServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {


    public static final String HTTP_DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss zzz";
    public static final String HTTP_DATE_GMT_TIMEZONE = "GMT";
    public static final int HTTP_CACHE_SECONDS = 60;


    PortalPortContext portContext;

    private NettyHttpRequestContext  requestContext     = new NettyHttpRequestContext();
    private NettyHttpResponseContext responseContext    = new NettyHttpResponseContext();

    private HttpRequest                             request;

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    public PortalPortContext getPortContext() {
        return portContext;
    }

    public void setPortContext(PortalPortContext portContext) {
        this.portContext = portContext;
    }

    private AbstractRequestFailContext getFail(String code, String message, Exception e) {
        AbstractRequestFailContext result = new AbstractRequestFailContext();
        result.setCode(code);
        result.setMessage(message);
        result.setExceiption(e);
        result.setRequest(requestContext);
        return result;
    }


    HttpStaticFileServerHandler httpStaticFileServerHandler;
    private HttpStaticFileServerHandler getHttpStaticFileServerHandler() throws Exception {
        if ( httpStaticFileServerHandler == null ) {
            httpStaticFileServerHandler = new HttpStaticFileServerHandler();
        }
        throw new Exception("Implmeneted file utils set");
/*
        httpStaticFileServerHandler = gets
        return httpStaticFileServerHandler;
*/
    }

    Logger logger;

    public Logger getLogger() {
        if ( logger == null ) {
            logger = Core.getLogger(this.getClass());
        }
        return logger;
    }

    private void logDebug(String msg) {
        getLogger().debug(msg);
    }

    protected void prepareRequestContext(HttpRequest request) {

        requestContext  = new NettyHttpRequestContext();
        responseContext = new NettyHttpResponseContext();

        requestContext.setResponse(responseContext);
        responseContext.setRequest(requestContext);

        AbstractSiteContext siteContext = portContext.getSites().get(0);
        QueryStringDecoder queryStringDecoder = new QueryStringDecoder(request.getUri());
        String cookieString = request.headers().get(COOKIE);
        if (cookieString != null) {
            for(io.netty.handler.codec.http.cookie.Cookie nettyCookie : ServerCookieDecoder.STRICT.decode(cookieString)) {
                Cookie cookie = new Cookie(nettyCookie.name(), nettyCookie.value());
                cookie.setDomain(nettyCookie.domain());
                cookie.setPath(nettyCookie.path());
                cookie.setMaxAge((int) nettyCookie.maxAge());
                cookie.setSecure(nettyCookie.isSecure());
                cookie.setHttpOnly(nettyCookie.isHttpOnly());
                requestContext.getResponse().setCookie(cookie);
            }
        }

        PortalSession portalSession = null;
//        logDebug("PATH                 " + request.getUri());
        String sessionId      = requestContext.getCookieValue(siteContext.getSessionIdParameterName());
//        logDebug("Request session id   " + sessionId);
        portalSession = siteContext.getSession(sessionId);
        if ( portalSession == null ) {
            portalSession = siteContext.newSession();
            responseContext.setCookie(portalSession.getSessionCookie());
        }

        requestContext.setPortalSession(portalSession);
        requestContext.setMethod(HttpMethod.toMethod(request.getMethod().toString()));
        if ( requestContext.getMethod() == HttpMethod.POST) {

            HttpPostRequestDecoder decoder = new HttpPostRequestDecoder(new DefaultHttpDataFactory(false), request);

            for(InterfaceHttpData data : decoder.getBodyHttpDatas() ) {
                if (data.getHttpDataType() == InterfaceHttpData.HttpDataType.Attribute) {
                    Attribute attribute = (Attribute) data;
                    try {
                        String value = attribute.getValue();
                        String[] values = new String[]{value};
                        requestContext.getParametersHashMap().put(attribute.getName(), values);
                    } catch (IOException e) {
                        throw new CoreException("Core exception on get attribute '?' value : ?", new Object[]{attribute.getName(), e.getMessage()}, e);
                    }
                }
            }
        }
        requestContext.setUri(request.getUri());


        requestContext.setPath(queryStringDecoder.path());

        for (Map.Entry<String, String> h: request.headers()) {
            String key = h.getKey();
            String value = h.getValue();
            requestContext.getHeaders().put(key, value);
        }

        for (String name : queryStringDecoder.parameters().keySet()) {
            List<String> values = queryStringDecoder.parameters().get(name);
            String[] sa = new String[values.size()];
            for (int i=0; i < sa.length; i++) {
                sa[i] = values.get(i);
            }
            requestContext.getParametersHashMap().put(name, sa);
        }

        requestContext.prepare();
        responseContext.prepare();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg) {

        if (msg instanceof HttpRequest) {

            request                          = msg;
            AbstractRequestFailContext fail  = null;

            try {

                prepareRequestContext(request);

                if ( requestContext.getLocaleContext() == null ) {
                    sendRedirect(ctx, "/" + requestContext.getSiteContext().getDefaultLocale().getLanguage() + request.getUri());
                    return;
                }


            } catch (CoreException e) {
                fail = getFail("500", e.getMessage(), e);
            }

            if ( fail == null ) {

                ServletContext servletContext = requestContext.getServletContext();

                if ( servletContext != null ) {
                    if ( servletContext instanceof FilesServletContext ) {

                        FilesServletContext filesServletContext = (FilesServletContext) servletContext;
                        String path = requestContext.getPath();
                        if ( requestContext.getLocaleContext() != null ) {
                            path = path.substring(1 + requestContext.getLocaleContext().getLanguage().length());
                        }
                        String requestFileName = path.substring(servletContext.getPath().length());

                        String localFileName   = filesServletContext.getLocalPath() + requestFileName;

                        try {
                            getHttpStaticFileServerHandler().setLocalFileName(localFileName);
                            getHttpStaticFileServerHandler().channelRead0(ctx, msg);
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                            fail = getFail("500", e.getMessage(), e);
                        }

                    } else {
                        AbstractServlet servlet = null;
                        try {
                            servlet = requestContext.getPortalSession().lockServlet(servletContext);
                            if ( servlet instanceof AbstractHttpServlet) {
                                ((AbstractHttpServlet) servlet).service(requestContext);
                            } else {
                                fail = getFail("500", "Servlet '" + servletContext.getPath() + " class '" + servlet.getClass().getName() + "' not inherited from AbstractHttpServlet" , null);
                            }
                        } catch (CoreException e) {
                            fail = getFail("500", e.getMessage(), e);
                        } finally {
                            if ( servlet != null ) {
                                servlet.releaseLock();
                            }
                        }
                    }

                } else {
                    sendError(ctx, NOT_FOUND);
                    return;

//                    fail = getFail("404", "Path  not found", null);
                }

            }


            if ( fail != null ) {
                responseContext.write("Error " + fail.getCode() + " : " + fail.getMessage() + "\r\n\r\n<br><br>");

                responseContext.write("HOSTNAME      : " + requestContext.getHeader(HOST) + "\r\n<br>");
                responseContext.write("REQUEST_URI   : " + requestContext.getUri() + "\r\n<br>");
                responseContext.write("PATH          : " + requestContext.getPath() + "\r\n<br>");

                if (requestContext.getHeaders().size() > 0) {
                    for (Map.Entry<String, String> h: requestContext.getHeaders().entrySet()) {
                        String key = h.getKey();
                        String value = h.getValue();
                        responseContext.write("HEADER: " + key + " = " + value + "\r\n<br>");
                    }
                    responseContext.write("\r\n");
                }

                if ( requestContext.getParametersHashMap().size() > 0) {
                    for (Map.Entry<String, String[]> p: requestContext.getParametersHashMap().entrySet()) {
                        String key    = p.getKey();
                        String[] vals = p.getValue();
                        for (String val : vals) {
                            responseContext.write("PARAM: " + key + " = " + val + "\r\n<br>");
                        }
                    }
                    responseContext.write("\r\n<br>");
                }

            }

            appendDecoderResult(requestContext);
        }

        if (msg instanceof HttpContent) {
            HttpContent httpContent = (HttpContent) msg;

            ByteBuf content = httpContent.content();
            if (content.isReadable()) {
/*
                responseContext.write("CONTENT: ");
                responseContext.write(content.toString(CharsetUtil.UTF_8));
                responseContext.write("\r\n");
*/
                appendDecoderResult(requestContext);
            }

            if (msg instanceof LastHttpContent) {

//                requestContext.write("END OF CONTENT\r\n");

                LastHttpContent trailer = (LastHttpContent) msg;
/*
                if (!trailer.trailingHeaders().isEmpty()) {
                    requestContext.write("\r\n");
                    for (CharSequence name: trailer.trailingHeaders().names()) {
                        for (CharSequence value: trailer.trailingHeaders().getAll(name)) {
                            requestContext.write("TRAILING HEADER: ");
                            requestContext.write(name.toString()).write(" = ").write(value.toString()).write("\r\n");
                        }
                    }
                    requestContext.write("\r\n");
                }
*/

                if (! writeResponse(trailer, ctx)) {
                    // If keep-alive is off, close the connection once the content is fully written.
                    ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
                }
            }
        }

    }

    private void appendDecoderResult(AbstractHttpRequestContext requestContext) {
        DecoderResult result = request.getDecoderResult();
        if (result.isSuccess()) {
            return;
        }
        responseContext.write(".. WITH DECODER FAILURE: ");
        responseContext.write(result.cause().getMessage());
        responseContext.write("\r\n");
    }

    private void writeCookies(FullHttpResponse response) {
        // Encode the cookie.
        for (Cookie cookie : responseContext.getCookies().values()) {

            io.netty.handler.codec.http.cookie.Cookie nettyCookie = new DefaultCookie(cookie.getName(), cookie.getValue());

            nettyCookie.setDomain(cookie.getDomain());
            nettyCookie.setPath(cookie.getPath());
            nettyCookie.setMaxAge(cookie.getMaxAge());
            nettyCookie.setHttpOnly(cookie.isHttpOnly());

            response.headers().add(SET_COOKIE, ServerCookieEncoder.STRICT.encode(nettyCookie));
        }

    }

    private boolean writeResponse(HttpObject currentObj, ChannelHandlerContext ctx) {
        // Decide whether to close the connection or not.
        boolean keepAlive = HttpHeaders.isKeepAlive(request);
        // Build the response object.

        HttpResponseStatus status = HttpResponseStatus.valueOf(responseContext.getStatus().code());

        FullHttpResponse response = new DefaultFullHttpResponse(
                HTTP_1_1
                ,currentObj.getDecoderResult().isSuccess() ? status : BAD_REQUEST
                ,Unpooled.copiedBuffer(responseContext.getBuffer().toString()
                ,CharsetUtil.UTF_8));

        response.headers().set(CONTENT_TYPE,
                (responseContext.getContentType() != null ? responseContext.getContentType() : "text/plain")
                + "; charset=" + (responseContext.getCharset() != null ? responseContext.getCharset() : "UTF-8")
        );

        if (keepAlive) {
            response.headers().set(CONTENT_LENGTH, response.content().readableBytes());
            response.headers().set(CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
        }

        for (Header header : responseContext.getHeaders().values()) {
            response.headers().set(header.getName(), header.getValue());
        }

        writeCookies(response);


        ctx.write(response);

        return keepAlive;
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

    private static void sendError(ChannelHandlerContext ctx, HttpResponseStatus status) {
        FullHttpResponse response = new DefaultFullHttpResponse(
                HTTP_1_1, status, Unpooled.copiedBuffer("Failure: " + status + "\r\n", CharsetUtil.UTF_8));
        response.headers().set(CONTENT_TYPE, "text/plain; charset=UTF-8");

        // Close the connection as soon as the error message is sent.
        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

    private void sendRedirect(ChannelHandlerContext ctx, String newUri) {

        FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, FOUND);

        response.headers().set(LOCATION, newUri);

        writeCookies(response);

        // Close the connection as soon as the error message is sent.
        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

}

