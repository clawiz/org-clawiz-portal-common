/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.engine.netty.http;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.stream.ChunkedWriteHandler;
import org.clawiz.core.common.Core;

import org.clawiz.core.common.system.logger.Logger;
import org.clawiz.portal.common.PortalPortContext;

/**
 *
 */
public class NettyServerHttpInitializer extends ChannelInitializer<SocketChannel> {

    PortalPortContext portContext;


    public PortalPortContext getPortContext() {
        return portContext;
    }

    public void setPortContext(PortalPortContext portContext) {
        this.portContext = portContext;
    }

    Logger logger;

    public Logger getLogger() {
        if ( logger == null ) {
            logger = Core.getLogger(this.getClass());
        }
        return logger;
    }

    private void logDebug(String msg) {
        getLogger().debug(msg);
    }

    @Override
    public void initChannel(SocketChannel ch) throws Exception {

//        logDebug("Channel initialized " + ch.config());

        ChannelPipeline p = ch.pipeline();

        NettyHttpServerHandler httpServerHandler = new NettyHttpServerHandler();
        httpServerHandler.setPortContext(portContext);

        p.addLast(new HttpServerCodec());
        p.addLast(new HttpObjectAggregator(65536));
        p.addLast(new ChunkedWriteHandler());
        p.addLast(httpServerHandler);
    }



}
