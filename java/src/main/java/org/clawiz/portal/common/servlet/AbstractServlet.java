/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet;


import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.utils.StringUtils;

/**
 *
 */
public class AbstractServlet extends Service {

    public String getPath() {
        return null;
    }

    private boolean locked = false;

    public void getLock() {
        locked = true;
    }

    public void releaseLock() {
        locked = false;
    }

    public boolean isLocked() {
        return locked;
    }

    /**
     * Return 'true' if serlvet can be called concurrently at one time in some portal session
     * override to return 'false' for servlets that perform operations that MUST by finished before next call
     * @return
     */
    public boolean isAllowedSessionConcurrentCalls() {
        return true;
    }

    /**
     * Return servlet secure mode
     * Non-registered users attempting to access servlet, redirected to login page (whe allowed - for example at http calls)
     * @see org.clawiz.portal.common.site.AbstractSiteContext#getLoginPath()
     * @return
     */
    public ServletSecureMode getSecureMode() {
        return ServletSecureMode.ALLOW_ALL;
    }

    /**
     * If true - parameter or cookie with portal session id must be defined in http request
     *
     * if not overriden, return true for all servlets with getPath like '/api/*'
     * @return
     */
    public boolean isSessionIdRequired() {
        return StringUtils.isLike(getPath(), "/api/*");
    }

}
