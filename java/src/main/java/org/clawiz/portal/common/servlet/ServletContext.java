/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet;

import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.portal.common.AbstractContext;
import org.clawiz.portal.common.site.AbstractSiteContext;
import org.clawiz.portal.common.site.application.AbstractApplicationContext;

/**
 *
 */
public class ServletContext extends AbstractContext {

    @NotInitializeService
    AbstractSiteContext        siteContext;

    @NotInitializeService
    AbstractApplicationContext applicationContext;

    Class               servletClass;

    String              path;

    ServletSecureMode   secureMode;

    boolean             sessionIdRequired;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public AbstractSiteContext getSiteContext() {
        return siteContext;
    }
    public void setSiteContext(AbstractSiteContext siteContext) {
        this.siteContext = siteContext;
    }

    public Class getServletClass() {
        return servletClass != null ? servletClass : this.getClass();
    }

    public <T extends AbstractServlet> void setServletClass(Class<T> servletClass, Session session) {
        this.servletClass       = servletClass;
        AbstractServlet servlet = session.getService(servletClass);
        this.secureMode         = servlet.getSecureMode();
        this.sessionIdRequired  = servlet.isSessionIdRequired();
    }

    public AbstractApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(AbstractApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public ServletSecureMode getSecureMode() {
        return secureMode;
    }

    public boolean isSessionIdRequired() {
        return sessionIdRequired;
    }
}
