/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet.html;


import org.clawiz.portal.common.servlet.http.AbstractHttpServlet;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;

public class AbstractHtmlPageServlet extends AbstractHttpServlet {

    public String getPageTitle(AbstractHttpRequestContext context) {
        return context.getSiteContext().getFullName();
    }

    public void pageOpen(AbstractHttpRequestContext context) {
        context.write("<html lang=\"en\">\n");
    }

    public void head(AbstractHttpRequestContext context) {
        context.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\n");
        context.write("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\n");
        context.write("<title>" + getPageTitle(context) + "</title>\n");
    }

    public void bodyOpen(AbstractHttpRequestContext context) {
        context.write("<body>");
    }

    public void bodyContent(AbstractHttpRequestContext context) {
    }

    public void bodyClose(AbstractHttpRequestContext context) {
        context.writeln("</body>");
    }

    public void pageClose(AbstractHttpRequestContext context) {
        context.writeln("</html>");
    }

    public void page(AbstractHttpRequestContext context) {
        context.write("<!DOCTYPE html>\n");
        pageOpen(context);
        context.write("<head>\n");
        head(context);
        context.write("\n");
        context.write("</head>\n");
        bodyOpen(context);
        context.write("\n");
        bodyContent(context);
        context.write("\n");
        bodyClose(context);
        context.write("\n");
        pageClose(context);
    }

    @Override
    public void doGet(AbstractHttpRequestContext context) {
        page(context);
    }

    @Override
    public void doPost(AbstractHttpRequestContext context) {
        page(context);
    }
}
