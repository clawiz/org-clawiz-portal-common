/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet.http;


import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import org.clawiz.portal.common.servlet.AbstractServlet;
import org.clawiz.portal.common.servlet.ServletSecureMode;
import org.clawiz.portal.common.servlet.http.api.response.ApiResponseContext;
import org.clawiz.portal.common.servlet.http.html.datamodel.AbstractDataModel;
import org.clawiz.portal.common.servlet.http.html.datamodel.ApplicationDataModel;
import org.clawiz.portal.common.servlet.http.html.datamodel.SiteDataModel;
import org.clawiz.portal.common.servlet.http.html.datamodel.UserDataModel;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;
import org.clawiz.portal.common.servlet.http.protocol.HttpResponseStatus;

/**
 *
 */
public class AbstractHttpServlet extends AbstractServlet {

    public static final String METHOD_DELETE               = "DELETE";
    public static final String METHOD_HEAD                 = "HEAD";
    public static final String METHOD_GET                  = "GET";
    public static final String METHOD_OPTIONS              = "OPTIONS";
    public static final String METHOD_POST                 = "POST";
    public static final String METHOD_PUT                  = "PUT";
    public static final String METHOD_TRACE                = "TRACE";

    public static final String HEADER_IFMODSINCE           = "If-Modified-Since";
    public static final String HEADER_LASTMOD              = "Last-Modified";

    public static final String COOKIE_REDIRECT_AFTER_LOGIN = "CLAWIZ_REDIRECT_AFTER_LOGIN";


    public static final String CONTENT_TYPE_MESSAGEHTTP    = "message/http";
    public static final String CONTENT_TYPE_TEXTHTML       = "text/html";

    private static final Gson gson = new Gson();


    public void doGet(AbstractHttpRequestContext context) {
        throwException("Method GET not implemented");
    }

    public void doHead(AbstractHttpRequestContext context) {
        throwException("Method HEAD not implemented");
    }

    public void doPost(AbstractHttpRequestContext context) {
        throwException("Method POST not implemented");
    }

    public void doPut(AbstractHttpRequestContext context) {
        throwException("Method PUT not implemented");
    }

    public void doDelete(AbstractHttpRequestContext context) {
        throwException("Method DELETE not implemented");
    }

    public void doOptions(AbstractHttpRequestContext context) {
        throwException("Method OPTIONS not implemented");
    }

    public void doTrace(AbstractHttpRequestContext context) {
        throwException("Method TRACE not implemented");
    }

    public void doPatch(AbstractHttpRequestContext context) {
        throwException("Method PATCH not implemented");
    }

    public void doConnect(AbstractHttpRequestContext context) {
        throwException("Method CONNECT not implemented");
    }

    /**
     * Override this method for change default redirection to login page for secured servlets
     * @param context
     * @return
     */
    protected boolean checkSecure(AbstractHttpRequestContext context) {
        if ( getSecureMode() == ServletSecureMode.REGISTERED_USERS && ! context.getPortalSession().isRegistered() ) {
            context.getResponse().setCookieValue(COOKIE_REDIRECT_AFTER_LOGIN, context.getUri());
            sendApplicationRedirect(context, context.getSiteContext().getLoginPath());
            return false;
        }
        return true;
    }

    public void service(AbstractHttpRequestContext context) {

        if ( ! checkSecure(context) ) {
            return;
        }

        switch (context.getMethod()) {
            case GET     : doGet(context);     break;
            case POST    : doPost(context);    break;
            case PUT     : doPut(context);     break;
            case HEAD    : doHead(context);    break;
            case DELETE  : doDelete(context);  break;
            case OPTIONS : doOptions(context); break;
            case TRACE   : doTrace(context);   break;
            case PATCH   : doPatch(context);   break;
            case CONNECT : doConnect(context); break;
            default: throwException("HTTP method '?' not implemented", new Object[]{context.getMethod().toString()});
        }

    }

    public void write(AbstractHttpRequestContext context, String line) {
        context.write(line);
    }

    public void write(AbstractHttpRequestContext context, ApiResponseContext apiResponseContext) {
        context.write(apiResponseContext.toJson().toString());
    }

    public void writeln(AbstractHttpRequestContext context, String line) {
        context.write(line);
        context.write("\n");
    }

    protected <T extends AbstractDataModel> T getTemplateDataModel(AbstractHttpRequestContext requestContext, Class<T> clazz) {
        T data = null;
        try {

            data = clazz.newInstance();
            UserDataModel user = new UserDataModel();

            data.setUser(user);
            user.setDisplayName(getSession().getUserName());
            user.setRegistered(getSession().isRegistered());

            SiteDataModel site = new SiteDataModel();
            site.setLanguage(requestContext.getLanguage());
            data.setSite(site);

            ApplicationDataModel application = new ApplicationDataModel();
            if ( requestContext != null && requestContext.getServletContext() != null
                    && requestContext.getServletContext().getApplicationContext() != null) {
                application.setPath(requestContext.getServletContext().getApplicationContext().getPath());
                data.setApplication(application);
            }


        } catch (Exception e) {
            throwException("Cannot create portal data instance for '?'", new Object[]{clazz});
        }

        return data;
    }

    /**
     * Send redirect inside application by adding application path to path parameter
     * @param context
     * @param path
     */
    protected void sendApplicationRedirect(AbstractHttpRequestContext context, String path) {
        context.getResponse().setHeader("Location", context.getServletContext().getApplicationContext().addApplicationPath(path));
        context.getResponse().setStatus(HttpResponseStatus.FOUND);
    }

    /**
     * Sendind redirect to specified external url
     * @param context
     * @param url
     */
    protected void sendExternalRedirect(AbstractHttpRequestContext context, String url) {
        context.getResponse().setHeader("Location", url);
        context.getResponse().setStatus(HttpResponseStatus.FOUND);
    }

    public <T> T fromJson(String json, Class<T> clazz)  {
        return gson.fromJson(json, clazz);
    }

    public JsonObject fromJson(String json) {
        return gson.fromJson(json, JsonElement.class).getAsJsonObject();
    }

    public String toJson(Object object) {
        return gson.toJson(object);
    }


}
