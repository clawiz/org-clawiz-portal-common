/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet.http.api.changepassword;

import com.google.gson.JsonObject;
import org.clawiz.core.common.storage.user.UserObject;
import org.clawiz.core.common.storage.user.UserService;
import org.clawiz.core.common.storage.user.WrongPreviousPasswordException;
import org.clawiz.portal.common.servlet.http.AbstractHttpServlet;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;

import java.math.BigDecimal;

public class ApiChangeUserPasswordServlet extends AbstractHttpServlet {

    public static final String PARAMETER_OLD_PASSWORD  = "oldPassword";
    public static final String PARAMETER_NEW_PASSWORD1 = "newPassword1";
    public static final String PARAMETER_NEW_PASSWORD2 = "newPassword2";

    @Override
    public String getPath() {
        return "/api/changeUserPassword";
    }

    UserService userService;

    @Override
    public void doPost(AbstractHttpRequestContext context) {

        String message = "";

        String oldPassword  = context.getParameterValue(PARAMETER_OLD_PASSWORD,  true);
        String newPassword1 = context.getParameterValue(PARAMETER_NEW_PASSWORD1, true);
        String newPassword2 = context.getParameterValue(PARAMETER_NEW_PASSWORD2, true);

        if ( ! newPassword1.equals(newPassword2)) {
            throwException("New passwords not equals");
        }


        UserObject   userObject = userService.load(getSession().getUserId());

        userObject.setPreviousPassword(oldPassword);
        userObject.setPassword(newPassword1);

        JsonObject response = new JsonObject();

        userObject.save();

        response.addProperty("status", "OK");
        response.addProperty("message", message);

        write(context, response.toString());
    }

}
