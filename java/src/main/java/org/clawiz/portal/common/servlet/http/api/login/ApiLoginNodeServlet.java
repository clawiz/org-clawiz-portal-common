/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet.http.api.login;

import com.google.gson.JsonObject;
import org.clawiz.core.common.storage.remotenode.RemoteNodeObject;
import org.clawiz.core.common.storage.remotenode.RemoteNodeService;
import org.clawiz.portal.common.servlet.http.AbstractHttpServlet;
import org.clawiz.portal.common.servlet.http.api.response.ApiResponseContext;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;

import java.math.BigDecimal;

public class ApiLoginNodeServlet extends AbstractHttpServlet {

    public static final String PARAMETER_NODENAME = "nodename";
    public static final String PARAMETER_PASSWORD = "password";

    @Override
    public String getPath() {
        return "/api/loginNode";
    }

    RemoteNodeService remoteNodeService;

    @Override
    public void doPost(AbstractHttpRequestContext context) {

        String status = ApiResponseContext.STATUS_OK;
        String message = "";

        String nodeName = context.getParameterValue(PARAMETER_NODENAME, true);
        String password = context.getParameterValue(PARAMETER_PASSWORD, true);

        if ( status.equals(ApiResponseContext.STATUS_OK)) {
            BigDecimal       nodeId     = remoteNodeService.nameToId(nodeName);
            RemoteNodeObject nodeObject;
            if ( nodeId == null ) {

                nodeObject = remoteNodeService.create();
                nodeObject.setName(nodeName);
                nodeObject.setPassword(password);
                nodeObject.save();
                nodeId = nodeObject.getId();
                commit();
                logDebug("New remote node registered : " + nodeName);

            } {
                nodeObject = remoteNodeService.load(nodeId);
            }

            if ( ! getSession().loginRemoteNode(nodeObject.getName(), password) ) {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                throwException("Wrong nodename or password");
            }
        }


        JsonObject response = new JsonObject();

        response.addProperty("status", status);
        response.addProperty("message", message);

        if ( getSession().getUserName() != null ) {
            response.addProperty("username", getSession().getUserName());
        }

        write(context, response.toString());
    }

}
