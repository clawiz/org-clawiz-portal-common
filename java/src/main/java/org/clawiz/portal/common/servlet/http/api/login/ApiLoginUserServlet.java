/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet.http.api.login;

import com.google.gson.JsonObject;
import org.clawiz.core.common.storage.user.UserObject;
import org.clawiz.core.common.storage.user.UserService;
import org.clawiz.portal.common.servlet.http.AbstractHttpServlet;
import org.clawiz.portal.common.servlet.http.api.response.ApiResponseContext;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;

import java.math.BigDecimal;

public class ApiLoginUserServlet extends AbstractHttpServlet {

    public static final String PARAMETER_USERNAME = "username";
    public static final String PARAMETER_PASSWORD = "password";

    @Override
    public String getPath() {
        return "/api/loginUser";
    }

    UserService userService;

    @Override
    public void doPost(AbstractHttpRequestContext context) {

        String message = "";

        String userName = context.getParameterValue(PARAMETER_USERNAME, true);
        String password = context.getParameterValue(PARAMETER_PASSWORD, true);

        BigDecimal   userId     = userService.nameToId(userName);
        UserObject   userObject = userId != null ? userService.load(userId) : null;

        if ( userObject == null || ! getSession().login(userObject.getName(), password) ) {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            throwException("Wrong username or password");
        }

        JsonObject response = new JsonObject();

        response.addProperty("status", "OK");
        response.addProperty("message", message);


        write(context, response.toString());
    }

}
