/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet.http.api.node;

import com.google.gson.JsonObject;
import org.clawiz.core.common.storage.remotenode.RemoteNodeObject;
import org.clawiz.core.common.storage.remotenode.RemoteNodeService;
import org.clawiz.portal.common.servlet.http.AbstractHttpServlet;
import org.clawiz.portal.common.servlet.http.api.response.ApiResponseContext;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;

import java.math.BigDecimal;

public class ApiResetNodeScnServlet extends AbstractHttpServlet {

    @Override
    public String getPath() {
        return "/api/resetNodeScn";
    }

    @Override
    public void doPost(AbstractHttpRequestContext context) {

        ApiResponseContext response = new ApiResponseContext();

        if ( getSession().getRemoteNode() == null ) {
            response.setStatus(ApiResponseContext.STATUS_ERROR);
            response.setMessage("Node not logged in");
        } else {
            response.setStatus(ApiResponseContext.STATUS_OK);
            RemoteNodeObject node = getSession().getRemoteNode();
            node.setLastReceivedScn(new BigDecimal(0));
            node.save();
        }

        write(context, response);
    }

}
