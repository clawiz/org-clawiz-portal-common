/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet.http.error;


import com.google.gson.JsonObject;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;
import org.clawiz.portal.common.servlet.http.protocol.HttpResponseStatus;

public class DefaultHttpErrorHandlerServlet extends AbstractHttpErrorHandlerServlet {


    protected boolean isApiCall(AbstractHttpRequestContext context) {
        String path = context.getPath();
        return path != null && path.length() > 4 && path.substring(0, 5).equalsIgnoreCase("/api/");
    }

    protected void writeJsonResponse(AbstractHttpRequestContext context,
                                     HttpResponseStatus status,
                                     String message) {
        if ( status == HttpResponseStatus.NOT_FOUND ) {
            return;
        }
        JsonObject response = new JsonObject();
        response.addProperty("status", "ERROR");
        response.addProperty("message", message);
        context.write(response.toString());
        context.getResponse().setStatus(HttpResponseStatus.OK);
    }

    protected void writeHtmlResponse(AbstractHttpRequestContext context,
                                     HttpResponseStatus status,
                                     String message) {

        ErrorDataModel dataModel = getErrorDataModel(context, status, message, ErrorDataModel.class);
        context.write( "\n" +
                "<html>\n" +
                "<head>\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=ISO-8859-1\"/>\n" +
                "<title>Error "+ dataModel.error.code+" </title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h2>HTTP ERROR: " + dataModel.error.code+ "</h2>\n" +
                "<p>Problem accessing " + context.getPath() + ". Reason:\n" +
                "<pre>    " + dataModel.error.reason +"</pre></p>\n" +
                (!StringUtils.isEmpty(message) ?
                        ("<p>Error message:\n" +
                                "<pre>    "+ message + "</pre></p>"
                        )
                        :
                        ""
                ) +
                "<hr />Powered by org.clawiz.Portal<hr/>\n" +
                "</body>\n" +
                "</html>");

    }



    public void doError(AbstractHttpRequestContext context,
                        HttpResponseStatus status,
                        String message) {

        context.getResponse().setStatus(status);
        context.getResponse().clearBuffer();

        if ( isApiCall(context)) {
            writeJsonResponse(context, status, message);
        } else {
            writeHtmlResponse(context, status, message);
        }
    }

}
