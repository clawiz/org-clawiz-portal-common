/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet.http.html.freemarker;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import org.clawiz.core.common.Core;

import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;
import org.clawiz.portal.common.servlet.http.AbstractHttpServlet;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abdrashitovta on 12.02.2015.
 */
public class AbstractFreemarkerHttpServlet extends AbstractHttpServlet {

    private final static Configuration _configuration = new Configuration(Configuration.VERSION_2_3_21);

    private static Configuration configuration;

    public static Configuration getConfiguration() {
        if ( configuration == null ) {
            synchronized (_configuration) {
                try {

                    ClassTemplateLoader ctl = new ClassTemplateLoader(Core.class, "/");
                    TemplateLoader[] loaders = new TemplateLoader[] { ctl };
                    MultiTemplateLoader mtl = new MultiTemplateLoader(loaders);
                    _configuration.setTemplateLoader(mtl);
                    _configuration.setDefaultEncoding("UTF-8");
                    _configuration.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                configuration = _configuration;
            }
        }
        return configuration;
    }

    public String getFreemarkerTemplatesPath() {
        throwException("Method 'getFreemarkerTemplatesPath' not implemented at class '?'", new Object[]{this.getClass().getName()});
        return null;
    }

    public void processFreemarkerTemplate(AbstractHttpRequestContext requestContext, Object model) {
        processFreemarkerTemplate(requestContext, getPath(), model);
    }

    public void processFreemarkerTemplate(AbstractHttpRequestContext requestContext, String templateFileName, Object model) {

        FreemarkerProcessContext freemarkerContext = new FreemarkerProcessContext();
        Map<String, Object> root = new HashMap<>();
        root.put("model", model);
        freemarkerContext.setDataModel(model);

        String languageDir   = requestContext.getLanguage();
        String templatesPath = getFreemarkerTemplatesPath();

        freemarkerContext.setTemplateFileName(templatesPath + "/languages/" + languageDir + "/" + templateFileName + ".ftl");

        Template template = null;
        try {
            template = getConfiguration().getTemplate(freemarkerContext.getTemplateFileName());
        } catch (Exception e) {
            try {
                freemarkerContext.setTemplateFileName(templatesPath + "/languages/common/" + templateFileName + ".ftl");
                template = getConfiguration().getTemplate(freemarkerContext.getTemplateFileName());
            } catch (Exception e1) {
                e.printStackTrace();
                throwException("Template for current language get exception '?' and common get error '?'", new Object[]{e.getMessage(), e1.getMessage()}, e);
            }
        }

        try {
            StringWriter writer = new StringWriter();
            template.process(freemarkerContext.getDataModel(), writer);
            write(requestContext, writer.toString());
        } catch (Exception e) {
            e.printStackTrace();
            throwException("Exception '?'", new Object[]{e.getMessage()}, e);
        }

    }

}
