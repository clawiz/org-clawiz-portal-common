/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet.http.html.login;


import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.portal.common.servlet.ServletSecureMode;
import org.clawiz.portal.common.servlet.http.html.freemarker.AbstractFreemarkerHttpServlet;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;

import javax.servlet.http.Cookie;

/**
 * (c) Clawiz
 */
public class LoginServlet extends AbstractFreemarkerHttpServlet {

    public static final String PARAMETER_USERNAME = "username";
    public static final String PARAMETER_PASSWORD = "password";

    @Override
    public boolean isAllowedSessionConcurrentCalls() {
        return false;
    }

    @Override
    public String getPath() {
        return "/login";
    }

    public String getLoginTemplateFileName(AbstractHttpRequestContext context) {
        return "login";
    }

    public String getLoginSuccessPath() {
        return "/";
    }

    @Override
    public ServletSecureMode getSecureMode() {
        return ServletSecureMode.ALLOW_ALL;
    }

    protected void writeLoginPage(AbstractHttpRequestContext context, LoginServletDataModel dataModel) {
        processFreemarkerTemplate(context, getLoginTemplateFileName(context), dataModel);
    }

    @Override
    public void doGet(AbstractHttpRequestContext context) {

        LoginServletDataModel dataModel = getTemplateDataModel(context, LoginServletDataModel.class);

        writeLoginPage(context, dataModel);

    }

    /**
     * Return value for "loginErrorMessage". Override for customization
     * @param context
     * @return
     */
    protected String getLoginFailedMessage(AbstractHttpRequestContext context) {
        return "Unknown username or password is wrong";
    }

    public LoginServletDataModel login(AbstractHttpRequestContext context) {
        String userName = context.getParameterValue(PARAMETER_USERNAME);
        String password = context.getParameterValue(PARAMETER_PASSWORD);

        LoginServletDataModel dataModel = getTemplateDataModel(context, LoginServletDataModel.class);
        if ( ! context.getPortalSession().login(userName, password) ) {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            dataModel.setLoginFailed(true);
            dataModel.setLoginErrorMessage(getLoginFailedMessage(context));
            dataModel.setUsername(userName);
        }

        return dataModel;
    }

    public void afterLogin(AbstractHttpRequestContext context) {
        sendApplicationRedirect(context, getLoginSuccessPath());
    }


    public void redirectSuccessLogin(AbstractHttpRequestContext context) {
        Cookie redirectCookie = context.getCookie(COOKIE_REDIRECT_AFTER_LOGIN);
        if ( redirectCookie != null && ! StringUtils.isEmpty(redirectCookie.getValue())) {
            Cookie setCookie = new Cookie(COOKIE_REDIRECT_AFTER_LOGIN, "");
            setCookie.setMaxAge(0);
            context.getResponse().setCookie(setCookie);
            sendApplicationRedirect(context, redirectCookie.getValue());
        } else {
            afterLogin(context);
        }
    }

    @Override
    public void doPost(AbstractHttpRequestContext context) {

        LoginServletDataModel dataModel = login(context);
        if ( dataModel.isLoginFailed() ) {
            writeLoginPage(context, dataModel);
            return;
        }

        redirectSuccessLogin(context);
    }
}

