/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet.http.protocol;


import org.clawiz.portal.common.servlet.AbstractRequestContext;
import org.clawiz.portal.common.servlet.ServletContext;
import org.clawiz.portal.common.site.LocaleContext;

import javax.servlet.http.Cookie;
import java.util.*;

/**
 *
 */
public abstract class AbstractHttpRequestContext extends AbstractRequestContext {

    ServletContext              servletContext;
    LocaleContext               localeContext;

    AbstractHttpResponseContext response;

    String                      lpad = "";

    HashMap<String, Object>     requestProcessData = new HashMap<>();

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public void setLocaleContext(LocaleContext localeContext) {
        this.localeContext = localeContext;
    }

    abstract public HttpMethod getMethod();

    abstract public String getUri();

    abstract public String getPath();

    abstract public String getHeader(String header);

    abstract public String   getParameterValue(String parameter);
    abstract public String   getParameterValue(String parameter, boolean throwErrorOnNotFound);

    abstract public String[] getParameterValues(String parameter);

    abstract public Cookie getCookie(String name);

    abstract public String getCookieValue(String name);

    public ServletContext getServletContext() {
        return servletContext;
    }

    public LocaleContext getLocaleContext() {
        return localeContext;
    }

    public String getLanguage() {
        return localeContext != null ? localeContext.getLanguage() : null;
    }

    public void prepare() {

        if ( localeContext == null ) {
            localeContext  = getSiteContext().getLocaleByRequest(this);
        }
        if (servletContext == null ) {
            servletContext = getSiteContext().getServletContextByRequest(this);
        }

    }

    public AbstractHttpResponseContext getResponse() {
        return response;
    }

    public void setResponse(AbstractHttpResponseContext response) {
        this.response = response;
    }

    public String getLpad() {
        return lpad;
    }

    public void setLpad(String lpad) {
        this.lpad = lpad;
    }

    public void write(String str) {
        response.buffer.append(lpad).append(str);
    }

    public void writeln(String str) {
        response.buffer.append(lpad).append(str).append("\n");
    }

    public void setProcessData(String name, Object value)  {
        this.requestProcessData.put(name, value);
    }

    public Object getProcessData(String name) {
        return name != null ? requestProcessData.get(name) : null;
    }

}
