/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet.http.protocol;


import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.portal.common.servlet.AbstractResponseContext;

import javax.servlet.http.Cookie;
import java.util.HashMap;

/**
 *
 */
public class AbstractHttpResponseContext extends AbstractResponseContext {

    String contentType = "text/html";
    String charset     = "utf-8";

    protected final StringBuilder buffer                  = new StringBuilder();
    HashMap<String, Cookie> cookies                       = new HashMap<>();
    HashMap<String, Header> headers                       = new HashMap<>();

    HttpResponseStatus status                             = HttpResponseStatus.OK;

    AbstractHttpRequestContext request;

    public void write(String str) {
        buffer.append(str);
    }
    public StringBuilder getBuffer() {
        return buffer;
    }
    public void clearBuffer() {
        buffer.setLength(0);
    }


    public Cookie setCookieValue(String name, String value) {
        if (StringUtils.isEmpty(name) ) {
            throw new CoreException("Cookie name cannot be empty");
        }
        Cookie cookie = cookies.get(name);
        if ( cookie == null ) {
            cookie = new Cookie(name, value);
            cookies.put(name, cookie);
        } else {
            cookie.setValue(value);
        }
        return cookie;
    }

    public void setCookie(Cookie cookie) {
        cookies.put(cookie.getName(), cookie);
    }

    public HashMap<String, Cookie> getCookies() {
        return cookies;
    }

    public HashMap<String, Header> getHeaders() {
        return headers;
    }

    public void setHeader(String header, String value) {
        headers.put(header, new Header(header, value));
    }

    public HttpResponseStatus getStatus() {
        return status;
    }

    public void setStatus(HttpResponseStatus status) {
        this.status = status;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public AbstractHttpRequestContext getRequest() {
        return request;
    }

    public void setRequest(AbstractHttpRequestContext request) {
        this.request = request;
    }

    public void prepare() {

    }
}
