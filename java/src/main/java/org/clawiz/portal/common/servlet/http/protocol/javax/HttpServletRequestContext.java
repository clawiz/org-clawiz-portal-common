/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.servlet.http.protocol.javax;


import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;
import org.clawiz.portal.common.servlet.http.protocol.HttpMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.*;

public abstract class HttpServletRequestContext extends AbstractHttpRequestContext {

    HttpServletRequest httpServletRequest;
    HttpMethod         httpMethod;

    public HttpServletRequest getHttpServletRequest() {
        return httpServletRequest;
    }

    public void setHttpServletRequest(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }

    @Override
    public void prepare() {
        super.prepare();
        httpMethod = HttpMethod.toMethod(httpServletRequest.getMethod());
    }

    @Override
    public HttpMethod getMethod() {
        return httpMethod;
    }

    @Override
    public String getHeader(String header) {
        return httpServletRequest.getHeader(header);
    }

    @Override
    public Cookie getCookie(String name) {
        if ( name == null ) {
            return null;
        }
        for(Cookie cookie : httpServletRequest.getCookies()) {
            if ( name.equals(cookie.getName())) {
                return cookie;
            }
        }
        return null;
    }

    @Override
    public String getCookieValue(String name) {
        Cookie cookie = getCookie(name);
        return cookie != null ? cookie.getValue() : null;
    }

    protected void prepareParameters() {
        parameterNames = new ArrayList<>();
        parameterValues = new HashMap<>();
        Enumeration<String> names = httpServletRequest.getParameterNames();
        while ( names.hasMoreElements() ) {
            String sourceName = names.nextElement();
            if ( sourceName != null ) {
                try {
                    String[] values = httpServletRequest.getParameterValues(sourceName);

                    String encodedSourceName = new String(sourceName.getBytes("ISO-8859-1"));

                    parameterValues.put(encodedSourceName, values);
                    parameterNames.add(encodedSourceName);
                } catch (UnsupportedEncodingException e) {
                    throw new CoreException("Unsupported encoding exception on parameter name '?'", new Object[]{sourceName});
                }
            }
        }
    }

    ArrayList<String>         parameterNames;
    HashMap<String, String[]> parameterValues;
    public ArrayList<String> getParameterNames() {
        if ( parameterNames == null ) {
            prepareParameters();
        }

        return parameterNames;
    }


    public HashMap<String, String[]> getParameterValues() {
        if ( parameterValues == null ) {
            prepareParameters();
        }

        return parameterValues;
    }

    public String[] getParameterValues(String name) {
        return getParameterValues().get(name != null ? name : null);
    }


    public String getParameterValue(String name) {

        String[] values = getParameterValues(name);
        if ( values != null && values.length > 1) {
            throw new CoreException("Parameter '?' has more than one value", name);
        }
        return values != null ? values[0] : null;
    }

    @Override
    public String getParameterValue(String parameter, boolean throwErrorOnNotFound) {
        String value = getParameterValue(parameter);
        if ( throwErrorOnNotFound && StringUtils.isEmpty(value)) {
            throw new CoreException("Parameter '?' must be defined", parameter);
        }
        return value;
    }
}
