/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.site;

import org.clawiz.core.common.Core;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.utils.RandomGUID;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.portal.common.AbstractContext;
import org.clawiz.portal.common.PortalContext;
import org.clawiz.portal.common.PortalSession;
import org.clawiz.portal.common.servlet.AbstractServlet;
import org.clawiz.portal.common.servlet.FilesServletContext;
import org.clawiz.portal.common.servlet.ServletContext;
import org.clawiz.portal.common.servlet.http.protocol.AbstractHttpRequestContext;
import org.clawiz.portal.common.site.application.AbstractApplicationContext;
import org.clawiz.portal.common.site.application.ApplicationContextList;

import javax.servlet.http.Cookie;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class AbstractSiteContext extends AbstractContext {

    @NotInitializeService
    PortalContext                    portalContext;

    ApplicationContextList           applicationContexts = new ApplicationContextList();

    ArrayList<ServletContext>        servlets = new ArrayList<>();
    ArrayList<FilesServletContext>   files    = new ArrayList<>();

    HashMap<String, ServletContext>  pathsCache = new HashMap<>();

    ArrayList<LocaleContext>         locales      = new ArrayList<>();
    HashMap<String, LocaleContext>   localesCache = new HashMap<>();

    LocaleContext                    defaultLocale = null;

    String                           siteName;
    int                              httpPort;
    String                           accessControlAllowOrigin;

    String                           loginPath             = "/login";
    String                           defaultSecuredPath    = "/";
    String                           defaultNotSecuredPath = "/";


    long                             sessionTimeout = 3600 * 1000;

    // Use ONLY for test purposes - all new site sessions will auto logged with this username
    String                          siteAutoLoginUserName;
    String                          siteAutoLoginUserPassword;

    public class DataSourceConnectionContext {

        boolean closeAfterCall    = true;
        boolean commitOnSuccess   = true;
        boolean rollbackOnFailure = true;

        public boolean isCloseAfterCall() {
            return closeAfterCall;
        }

        public void setCloseAfterCall(boolean closeAfterCall) {
            this.closeAfterCall = closeAfterCall;
        }

        public boolean isCommitOnSuccess() {
            return commitOnSuccess;
        }

        public void setCommitOnSuccess(boolean commitOnSuccess) {
            this.commitOnSuccess = commitOnSuccess;
        }

        public boolean isRollbackOnFailure() {
            return rollbackOnFailure;
        }

        public void setRollbackOnFailure(boolean rollbackOnFailure) {
            this.rollbackOnFailure = rollbackOnFailure;
        }
    }

    public class DataSourceContext {
        DataSourceConnectionContext connectionContext = new DataSourceConnectionContext();

        public DataSourceConnectionContext getConnectionContext() {
            return connectionContext;
        }
    }

    DataSourceContext dataSourceContext = new DataSourceContext();


    private ConcurrentHashMap<String, PortalSession> sessionsCache = new ConcurrentHashMap<>();

    String sessionIdParameterName = "cwsessionid";
    public String getSessionIdParameterName() {
        return sessionIdParameterName;
    }

    public PortalSession getSession(String id) {
        return StringUtils.isEmpty(id) ? null : sessionsCache.get(id);
    }

    public PortalSession newSession() {
        Session coreSession = Core.newSession();
        PortalSession portalSession = new PortalSession(coreSession);
        portalSession.setSiteContext(this);
        sessionsCache.put(portalSession.getKey(), portalSession);

        logDebug("Portal session created " + portalSession.getKey());

        Cookie cookie = new Cookie(getSessionIdParameterName(), portalSession.getKey());
        cookie.setPath("/");
        cookie.setHttpOnly(true);

        portalSession.setSessionCookie(cookie);

        return portalSession;
    }


    public Collection<PortalSession> getSessions() {
        return sessionsCache.values();
    }

    public void destroySession(String id) {
        PortalSession session = getSession(id);
        if ( session == null ) {
            throw new CoreException("Session '?' does not exists", new Object[]{id});
        }
        Core.getSessions().destroySession(session.getCoreSession());
        sessionsCache.remove(id);
    }

    public void putSession(String id, PortalSession session) {
        sessionsCache.put(id, session);
    }

    public PortalContext getPortalContext() {
        return portalContext;
    }

    public void setPortalContext(PortalContext portalContext) {
        this.portalContext = portalContext;
    }

    public DataSourceContext getDataSourceContext() {
        return dataSourceContext;
    }

    public void makeAddPath(ServletContext context) {

        if ( pathsCache.containsKey(context.getPath())) {
            ServletContext sc = pathsCache.get(context.getPath());

            throw new CoreException("Path '?' already defined for servlet '?' of site '?'", context.getPath(), sc.getServletClass().getName(), getFullName());
        }

        pathsCache.put(context.getPath(), context);

    }

    public LocaleContext getLocaleByPath(String path) {
        if ( path == null ) {
            return null;
        }
        if ( path.charAt(0) != '/' ) {
            return null;
        }
        int i = path.indexOf("/", 2);
        if ( i < 0 ) {
            return null;
        }

        return localesCache.get(path.substring(1, i).toUpperCase());
    }

    public LocaleContext getLocaleByRequest(AbstractHttpRequestContext request) {
        return getLocaleByPath(request.getPath());
    }

    public String removeLocaleFromPath(String path, LocaleContext localeContext) {
        if ( localeContext == null ) {
            return path;
        }
        String languagePrefix = "/" + localeContext.getLanguage();
        if (    path.length() >= languagePrefix.length() &&
                path.substring(0, languagePrefix.length()).equalsIgnoreCase(languagePrefix)) {
            return path.substring(languagePrefix.length());
        }
        return path;
    }

    public ServletContext getServletContextByPath(String path, LocaleContext localeContext) {
        if ( path == null ) {
            return null;
        }

        path = removeLocaleFromPath(path, localeContext);

        ServletContext context = pathsCache.get(path);
        if ( context != null ) {
            return context;
        }

        for (FilesServletContext fileContext : files) {
            if ( path.length() >= fileContext.getPath().length() ) {
                if ( path.substring(0, fileContext.getPath().length()).equalsIgnoreCase(fileContext.getPath())) {
                    return fileContext;
                }
            }
        }

        return null;
    }

    public ServletContext getServletContextByRequest(AbstractHttpRequestContext request) {
        return getServletContextByPath(request.getPath(), request.getLocaleContext());
    }

    private void addServlet(ServletContext context) {
        servlets.add(context);
        makeAddPath(context);
    }

    public <T extends AbstractServlet> ServletContext addServlet(Class<T> clazz, String path, AbstractApplicationContext applicationContext) {
        ServletContext context = getService(ServletContext.class, ServletContext.class.getName() + ":" + (new RandomGUID()).toString());
        context.setServletClass(clazz, getSession());
        context.setPath(path);
        context.setApplicationContext(applicationContext);
        addServlet(context);
        return context;
    }

    public ArrayList<ServletContext> getServlets() {
        return servlets;
    }

    public FilesServletContext addFiles(String localPath, String path) {
        FilesServletContext context = getService(FilesServletContext.class, FilesServletContext.class.getName() + ":" + (new RandomGUID()).toString());
        context.setPath(path);
        context.setLocalPath(localPath);
        addFiles(context);
        return context;
    }

    public void addFiles(FilesServletContext context) {
        files.add(context);
        makeAddPath(context);
    }

    public ArrayList<FilesServletContext> getFiles() {
        return files;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String name) {
        this.siteName = name;
    }

    public int getHttpPort() {
        return httpPort;
    }

    public void setHttpPort(int port) {
        this.httpPort = port;
    }

    public String getAccessControlAllowOrigin() {
        return accessControlAllowOrigin;
    }

    public void setAccessControlAllowOrigin(String accessControlAllowOrigin) {
        this.accessControlAllowOrigin = accessControlAllowOrigin;
    }

    public String getFullName() {
        return (portalContext != null ? portalContext.getName() + "." : "") + getSiteName();
    }

    public ArrayList<LocaleContext> getLocales() {
        return locales;
    }

    public LocaleContext getDefaultLocale() {
        return defaultLocale;
    }

    public void setDefaultLocale(LocaleContext defaultLocale) {
        this.defaultLocale = defaultLocale;
    }

    public void addLocale(LocaleContext locale) {
        String key = locale.getLanguage();
        if (StringUtils.isEmpty(key)  ) {
            throw new CoreException("Locale language must be defined");
        }

        key = key.toUpperCase();
        if ( localesCache.get(key) != null ) {
            return;
        }

        localesCache.put(key, locale);
        locales.add(locale);
    }

    public LocaleContext addLanguage(String language) {
        LocaleContext locale = new LocaleContext();
        locale.setLanguage(language);
        addLocale(locale);
        return locale;
    }


    /**
     * Return path to login page
     * If non-registered user attempt call servlet, than can be access only in secured mode, it will be redirected to this page and cookie 'CLAWIZ_REDIRECT_AFTER_LOGIN' will be set to user request URI
     * @see AbstractHttpRequestContext#getUri()
     * @return
     */
    public String getLoginPath() {
        return loginPath;
    }

    /**
     * Set path to site login page
     * If non-registered user attempt call servlet, than can be access only in secured mode, it will be redirected to this page and cookie 'CLAWIZ_REDIRECT_AFTER_LOGIN' will be set to user request URI
     * @see AbstractHttpRequestContext#getUri()
     * @param loginPath Path to sitelogin page
     */
    public void setLoginPath(String loginPath) {
        this.loginPath = loginPath;
    }

    /**
     *
     * @return Path to redirect after user successfully registered
     */
    public String getDefaultSecuredPath() {
        return defaultSecuredPath;
    }

    /**
     * Set path to redirect after user successfully registered.
     * @param defaultSecuredPath Path to redirect after user successfully registered.
     */
    public void setDefaultSecuredPath(String defaultSecuredPath) {
        this.defaultSecuredPath = defaultSecuredPath;
    }

    @Override
    public void prepare() {

        super.prepare();

        if ( locales.size() == 0 ) {
            LocaleContext locale = addLanguage("en");
            setDefaultLocale(locale);
        }

    }

    public void prepareApplications() {
        for(AbstractApplicationContext applicationContext : applicationContexts) {
            applicationContext.prepare();
        }
    }

    /**
     * Return path to redirect user after logout
     * @return
     */
    public String getDefaultNotSecuredPath() {
        return defaultNotSecuredPath;
    }

    public void setDefaultNotSecuredPath(String defaultNotSecuredPath) {
        this.defaultNotSecuredPath = defaultNotSecuredPath;
    }

    public long getSessionTimeout() {
        return sessionTimeout;
    }

    public void setSessionTimeout(long sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    public void addApplication(AbstractApplicationContext applicationContext) {
        applicationContext.setSiteContext(this);
        applicationContexts.add(applicationContext);
    }

    public ApplicationContextList getApplicationContexts() {
        return applicationContexts;
    }

    public String getSiteAutoLoginUserName() {
        return siteAutoLoginUserName;
    }

    /**
     * AutoLogin methods send site to ALWAYS log new session with defined user and password
     * Use it for test purposes only
     * @param siteUserName
     */
    public void setSiteAutoLoginUserName(String siteUserName) {
        this.siteAutoLoginUserName = siteUserName;
    }

    public String getSiteAutoLoginUserPassword() {
        return siteAutoLoginUserPassword;
    }

    /**
     * AutoLogin methods send site to ALWAYS log new session with defined user and password
     * Use it for test purposes only
     * @param siteAutoLoginUserPassword
     */
    public void setSiteAutoLoginUserPassword(String siteAutoLoginUserPassword) {
        this.siteAutoLoginUserPassword = siteAutoLoginUserPassword;
    }
}
