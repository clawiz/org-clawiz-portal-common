/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.portal.common.site.application;


import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.config.Config;
import org.clawiz.core.common.utils.RandomGUID;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.portal.common.AbstractContext;
import org.clawiz.portal.common.servlet.AbstractServlet;
import org.clawiz.portal.common.servlet.FilesServletContext;
import org.clawiz.portal.common.servlet.ServletContext;
import org.clawiz.portal.common.servlet.http.error.AbstractHttpErrorHandlerServlet;
import org.clawiz.portal.common.servlet.http.error.DefaultHttpErrorHandlerServlet;
import org.clawiz.portal.common.site.AbstractSiteContext;

public class AbstractApplicationContext extends AbstractContext {

    public static final String CONFIG_NAME = "name";
    public static final String CONFIG_PATH = "path";

    @NotInitializeService
    AbstractSiteContext siteContext;

    String path;
    String name;

    @NotInitializeService
    private ServletContext httpErrorHandler;

    public AbstractSiteContext getSiteContext() {
        return siteContext;
    }

    public void setSiteContext(AbstractSiteContext siteContext) {
        this.siteContext = siteContext;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    private static int appIndex = 0;

    @Override
    public void setConfig(Config config) {
        super.setConfig(config);

        String name = config.getString(CONFIG_NAME, false);
        if (name == null) {
            name = "APP" + (appIndex++);
        }
        setName(name);

        String path = config.getString(CONFIG_PATH, "");
        setPath(path);

    }

    public String addApplicationPath(String path) {
        boolean rootPath = StringUtils.isEmpty(path) || path.equals("/");
        String result;
        if ( rootPath ) {
            if ( StringUtils.isEmpty(this.path) || this.path.equals("/")) {
                return "/";
            }
            result = this.path;
        } else {
            result = this.path + "/" + path;
        }

        String replaced = result.replaceAll("//", "/");
        while ( ! replaced.equals(result)) {
            result = replaced;
            replaced = result.replaceAll("//", "/");
        }

        return result;
    }

    public FilesServletContext addFiles(String localPath, String path) {
        return getSiteContext().addFiles(localPath, addApplicationPath(path));
    }

    public <T extends AbstractServlet> ServletContext addServlet(Class<T> clazz, String path) {
        ServletContext servlet = getSiteContext().addServlet(clazz, addApplicationPath(path), this);
        return servlet;
    }

    public <T extends AbstractServlet> ServletContext addServlet(Class<T> clazz) {
        return addServlet(clazz, getService(clazz).getPath());
    }

    public ServletContext getHttpErrorHandler() {
        return httpErrorHandler;
    }

    public void setHttpErrorHandler(ServletContext httpErrorHandler) {
        this.httpErrorHandler = httpErrorHandler;
    }

    public <T extends AbstractHttpErrorHandlerServlet> void setHttpErrorHandler(Class<T> clazz) {
        ServletContext context = getService(ServletContext.class, ServletContext.class.getName() + ":" + (new RandomGUID()).toString());
        context.setServletClass(clazz, getSession());
        context.setApplicationContext(this);
        setHttpErrorHandler(context);
    }

    @Override
    public void prepare() {
        super.prepare();
        if ( httpErrorHandler == null ) {
            setHttpErrorHandler(DefaultHttpErrorHandlerServlet.class);
        }
    }
}